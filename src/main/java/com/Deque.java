package com;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<T> implements Iterable<T> {
    private Node head;
    private Node tail;

    public static void main(String[] args) {
        Deque<String> deque = new Deque<>();
        deque.addFirst("second");
        deque.addLast("third");
        deque.addFirst("first");
        deque.addFirst("first to be removed");
        deque.addLast("last to be removed");
        deque.removeFirst();
        deque.removeLast();
        System.out.println("size=" + deque.size() + " should be 3");
        System.out.println("isEmpty=" + deque.isEmpty() + " should be false");

        for (String s : deque) System.out.println(s);
    }

    public Node Deque$$head() {
        return this.head;
    }

    private void Deque$$head_$eq(Node x$1) {
        this.head = x$1;
    }

    private Node tail() {
        return this.tail;
    }

    private void tail_$eq(Node x$1) {
        this.tail = x$1;
    }

    private int _size = 0;

    private int _size() {
        return this._size;
    }

    private void _size_$eq(int x$1) {
        this._size = x$1;
    }

    public boolean isEmpty() {
        return (Deque$$head() == null);
    }


    public int size() {
        return _size();
    }


    public void addFirst(Object item) {
        if (item == null) throw new IllegalArgumentException();
        head = new Node((T) item, null, head);
        if (Deque$$head().next() == null) {
            tail_$eq(Deque$$head());
        } else {
            Deque$$head().next().prev = Deque$$head();
        }
        _size_$eq(_size() + 1);
    }


    public void addLast(Object item) {
        tail = new Node((T)item, tail(), null);

        if (tail().prev() == null) {
            Deque$$head_$eq(tail());
        } else {
            tail().prev().next = tail();
        }
        _size_$eq(_size() + 1);
    }


    public T removeFirst() {
        if (Deque$$head() == null) throw new NoSuchElementException();
        Node first = Deque$$head();
        Deque$$head_$eq(Deque$$head().next());
        if (Deque$$head() != null) Deque$$head().prev = null;
        if (Deque$$head() == null || Deque$$head().next() == null) tail_$eq(Deque$$head());
        _size_$eq(_size() - 1);
        return first.value();
    }


    public T removeLast() {
        if (tail() == null) throw new NoSuchElementException();
        Node last = tail();
        tail_$eq(tail().prev());
        if (tail() != null) tail().next = null;
        if (tail() == null || tail().prev() == null) Deque$$head_$eq(tail());
        _size_$eq(_size() - 1);
        return last.value();
    }


    public Iterator<T> iterator() {
        return new Deque$$anon$1(this);
    }

    public final class Deque$$anon$1 implements Iterator<T> {
        private Deque<T>.Node nextNode;

        public Deque$$anon$1(Deque $outer) {
            this.nextNode = $outer.Deque$$head();
        }

        private Deque<T>.Node nextNode() {
            return this.nextNode;
        }

        private void nextNode_$eq(Deque.Node x$1) {
            this.nextNode = x$1;
        }

        public boolean hasNext() {
            return (nextNode() != null);
        }

        public T next() {
            Deque.Node current = nextNode();
            nextNode_$eq(current.next());
            return (T) current.value();
        }
    }

    private class Node {
        private final T value;

        public Node(T value, Node prev, Node next) {
            this.value = value;
            this.prev = prev;
            this.next = next;
        }

        private Node prev;
        private Node next;

        public T value() {
            return this.value;
        }

        public Node prev() {
            return this.prev;
        }

        public Node next() {
            return this.next;
        }
    }


}
