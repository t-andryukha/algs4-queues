package com;

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdRandom;

public class Permutation {
    public static void main(String[] args) {
        Integer permutationLength = Integer.valueOf(args[0]);
        String inputString = StdIn.readString();
        Deque<Character> queue = new Deque<>();
        char[] chars = inputString.toCharArray();
        StdRandom.shuffle(chars);
        for (int i = 0; i < permutationLength; i++) {
            queue.addLast(chars[i]);
        }
        for (Character character : queue) {
            System.out.println(character);
        }
    }
}
