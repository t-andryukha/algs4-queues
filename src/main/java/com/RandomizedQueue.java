package com;

import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<T> implements Iterable<T> {
    private static final double MIN_LOADING_FACTOR = 0.25;
    private static final int EXPAND_FACTOR = 2;
    private static final int DEFAULT_CAPACITY = 10;

    public static void main(String[] args) {
        RandomizedQueue<String> randomizedQueue = new RandomizedQueue<>();
        randomizedQueue.enqueue("first");
        randomizedQueue.enqueue("second");
        randomizedQueue.enqueue("third");
        randomizedQueue.enqueue("fourth");
        randomizedQueue.enqueue("fifth");
        System.out.println("dequeuing " + randomizedQueue.dequeue());
        System.out.println("queue size " + randomizedQueue.size() + " should be 4");
        System.out.println("isEmpty=" + randomizedQueue.isEmpty() + " should be false");
        System.out.println("iterating over queue first time: ");
        for (String item : randomizedQueue) {
            System.out.println(item);
        }
        System.out.println("iterating over queue second time should bring different order: ");
        for (String item : randomizedQueue) {
            System.out.println(item);
        }
    }

    private Object[] items = new Object[DEFAULT_CAPACITY];
    private int start;
    private int end;

    private T dequeue() {
        if (isEmpty()) throw new NoSuchElementException();
        int randomIndex = randomIndex();
        Object randomItem = items[randomIndex];
        swap(items, randomIndex, start);
        start++;

        shrink();
        return (T) randomItem;
    }

    private void enqueue(T item) {
        if (item == null) throw new IllegalArgumentException();
        ensureCapacity();
        items[end] = item;
        end++;
        if (end == items.length) end = 0;
    }

    private boolean isUnderloaded() {
        return items.length > size() / MIN_LOADING_FACTOR;
    }

    private void shrink() {
        if (!isUnderloaded()) return;
        Object[] itemsResized = new Object[items.length / EXPAND_FACTOR];
        RandomizedQueue$$copyItems(itemsResized);
        items = itemsResized;
    }

    public T sample() {
        if (isEmpty()) throw new NoSuchElementException();
        return (T) items[randomIndex()];
    }

    private int randomIndex() {
        int randomIndex = start + StdRandom.uniform(size() - 1);
        if (randomIndex > items.length - 1) return randomIndex - (items.length - 1);
        return randomIndex;
    }

    private void swap(Object[] arr, int first, int second) {
        Object temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

    public int size() {
        return (end >= start) ? (end - start) : (
                items.length - 1 - start + end);
    }


    public boolean isEmpty() {
        return start == end;
    }


    private void ensureCapacity() {
        if (size() < items.length) return;
        Object[] itemsResized = new Object[items.length * EXPAND_FACTOR];
        RandomizedQueue$$copyItems(itemsResized);
        items = itemsResized;
    }

    public void RandomizedQueue$$copyItems(Object[] itemsResized) {
        int numOfItemsFromStartToArrayEnd = items.length - start;
        if (start < end) {
            System.arraycopy(items, start, itemsResized, 0, end - start);
        } else {
            System.arraycopy(items, start, itemsResized, 0, numOfItemsFromStartToArrayEnd - 1);
            System.arraycopy(items, 0, itemsResized, numOfItemsFromStartToArrayEnd, start - end);
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int current = 0;
            private Object[] iteratorItems = new Object[size()];

            {
                RandomizedQueue$$copyItems(iteratorItems);
                shuffle(iteratorItems);
            }

            private void shuffle(Object[] arr) {
                for (int i = 0; i < arr.length; i++) {
                    int randomIndex = StdRandom.uniform(0, arr.length - 1);
                    swap(arr, i, randomIndex);
                }
            }

            @Override
            public boolean hasNext() {
                return current < iteratorItems.length;
            }

            @Override
            public T next() {
                return (T) iteratorItems[current++];
            }
        };
    }
}
