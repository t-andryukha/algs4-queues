import java.util
import java.util.NoSuchElementException

object Deque {
  def main(args: Array[String]): Unit = {
    val deque = new Deque[String]()
    deque.addFirst("second")
    deque.addLast("third")
    deque.addFirst("first")
    deque.addFirst("first to be removed")
    deque.addLast("last to be removed")
    deque.removeFirst()
    deque.removeLast()
    println(s"size=${deque.size} should be 3")
    println(s"isEmpty=${deque.isEmpty} should be false")

    val iterator = deque.iterator()
    while (iterator.hasNext) {
      println(iterator.next())
    }
  }
}

/**
 * Memory footprint: 8+8+4+16 + N*(8 + 8*3 + 16)=~48N + 36
 */
class Deque[T] extends java.lang.Iterable[T] {

  private var head: Node = _
  private var tail: Node = _
  private var _size: Int = 0

  def isEmpty: Boolean = head == null

  // return the number of items on the deque
  def size: Int = _size

  // add the item to the front
  def addFirst(item: T): Unit = {
    if(item==null) throw new IllegalArgumentException()
    head = Node(item, null, head)
    if (head.next == null) {
      tail = head
    } else {
      head.next.prev = head
    }
    _size += 1
  }

  // add the item to the back
  def addLast(item: T): Unit = {
    tail = Node(item, tail, null)
    if (tail.prev == null) {
      head = tail
    } else {
      tail.prev.next = tail
    }
    _size += 1
  }

  // remove and return the item from the front
  def removeFirst(): T = {
    if (head == null) throw new NoSuchElementException()
    val first = head
    head = head.next
    if (head != null) head.prev = null
    if (head == null || head.next == null) tail = head
    _size -= 1
    first.value
  }

  // remove and return the item from the back
  def removeLast(): T = {
    if (tail == null) throw new NoSuchElementException()
    val last = tail
    tail = tail.prev
    if (tail != null) tail.next = null
    if (tail == null || tail.prev == null) head = tail
    _size -= 1
    last.value
  }

  // return an iterator over items in order from front to back
  def iterator(): java.util.Iterator[T] = new util.Iterator[T] {
    var nextNode: Node = head

    override def hasNext: Boolean = nextNode != null

    override def next(): T = {
      val current = nextNode
      nextNode = current.next
      current.value
    }
  }


  case class Node(value: T, var prev: Node, var next: Node)

}
