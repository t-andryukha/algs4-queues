import RandomizedQueue.{DefaultCapacity, ExpandFactor}
import edu.princeton.cs.algs4.StdRandom

import scala.reflect.ClassTag

object RandomizedQueue {

  val DefaultCapacity = 10;
  val ExpandFactor = 2
  val MinLoadingFactor = 0.25

  def main(args: Array[String]): Unit = {
    val randomizedQueue = new RandomizedQueue[String]
    randomizedQueue.enqueue("first")
    randomizedQueue.enqueue("second")
    randomizedQueue.enqueue("third")
    randomizedQueue.enqueue("fourth")
    randomizedQueue.enqueue("fifth")
    println(s"dequeuing ${randomizedQueue.dequeue}")
    println(s"queue size ${randomizedQueue.size} should be 4")
    println(s"isEmpty=${randomizedQueue.isEmpty} should be false")
    println(s"iterating over queue first time: ")
    for (item <- randomizedQueue) {
      println(item)
    }
    println(s"iterating over queue second time should bring different order: ")
    for (item <- randomizedQueue) {
      println(item)
    }

  }


}

class RandomizedQueue[T](implicit classTag: ClassTag[T]) extends Iterable[T] {

  implicit class RichArray(val arr: Array[T]) {
    /**
     * Swap
     */
    def <=>(first: Int, second: Int): Unit = {
      val temp = arr(first)
      arr(first) = arr(second)
      arr(second) = temp
    }

    def shuffle(): Unit = {
      for (i <- arr.indices) {
        arr <=> (i, StdRandom.uniform(0, arr.length - 1))
      }
    }
  }

  private var items: Array[T] = classTag.newArray(DefaultCapacity)
  private var start: Int = 0
  private var end: Int = 0

  //Iterator.  Each iterator must return the items in uniformly random order.
  // The order of two or more iterators to the same randomized queue must be mutually independent;
  // each iterator must maintain its own random order.
  override def iterator: Iterator[T] = {
    new Iterator[T] {

      private val iteratorItems: Array[T] = {
        val theItems = new Array[T](RandomizedQueue.this.size)
        RandomizedQueue.this.copyItems(theItems)
        theItems.shuffle()
        theItems
      }
      private var current = 0

      override def hasNext: Boolean = current < iteratorItems.length

      override def next(): T = {
        val item = iteratorItems(current)
        current += 1
        item
      }
    }
  }

  // return the number of items on the randomized queue
  override def size: Int = {
    if (end >= start) end - start
    else items.length - 1 - start + end
  }

  // is the randomized queue empty?
  override def isEmpty: Boolean = start == end


  private def ensureCapacity(): Unit = {
    if (size < items.length) return
    val itemsResized = classTag.newArray(items.length * ExpandFactor)
    copyItems(itemsResized)
    items = itemsResized
  }

  private def copyItems(itemsResized: Array[T]): Unit = {
    val numOfItemsFromStartToArrayEnd = items.length - start
    if (start < end) {
      System.arraycopy(items, start, itemsResized, 0, end - start)
    } else {
      System.arraycopy(items, start, itemsResized, 0, numOfItemsFromStartToArrayEnd - 1)
      System.arraycopy(items, 0, itemsResized, numOfItemsFromStartToArrayEnd, start - end)
    }
  }

  // add the item
  def enqueue(item: T): Unit = {
    if (item == null) throw new IllegalArgumentException()
    ensureCapacity()
    items(end) = item
    end += 1
    if (end == items.length) end = 0
  }

  private def isUnderloaded = items.length > size / RandomizedQueue.MinLoadingFactor

  private def shrink(): Unit = {
    if (!isUnderloaded) return
    val itemsResized = classTag.newArray(items.length / ExpandFactor)
    copyItems(itemsResized)
    items = itemsResized
  }

  // remove and return a random item
  def dequeue: T = {
    if (isEmpty) throw new NoSuchElementException()
    val randomItem = items(randomIndex)
    items <=> (randomIndex, start)
    items(start) = _
    start += 1
    shrink()
    randomItem
  }

  // return a random item (but do not remove it)
  def sample: T = {
    if (isEmpty) throw new NoSuchElementException()
    items(randomIndex)
  }

  private def randomIndex = {
    val lower = Math.min(start, end)
    val upper = Math.max(start, end)
    val randomIndex = StdRandom.uniform(lower, upper)
    randomIndex
  }


}

